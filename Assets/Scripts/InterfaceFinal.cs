﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceFinal : MonoBehaviour {

	public AudioSource audio;
	public AudioClip victoryAudio;
	public AudioClip loseAudio;
	public Text textCoins;
	public Text textTime;
	
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();

		if (GameManager.collectedCoins == GameManager.totalCoins) {
			audio.PlayOneShot(victoryAudio);
		} else {
			audio.PlayOneShot(loseAudio);	
		}
		
		audio.Play();
		textCoins.text = GameManager.collectedCoins + " / " + GameManager.totalCoins;
		textTime.text = GameManager.time + " sec";
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void OpenMenu() {
		Application.LoadLevel("MenuScene");
	}

	public void Exit() {
	}
}
