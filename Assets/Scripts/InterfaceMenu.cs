﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceMenu : MonoBehaviour {

	public AudioSource audio;
	
	// Use this for initialization
	void Start () {
		GameManager.collectedCoins = 0;
		GameManager.health = 100;
		GameManager.power = 0;
		GameManager.time = 0;
		audio = GetComponent<AudioSource>();
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenGame() {
		Application.LoadLevel("SampleScene");
	}
	
	public void OpenHowToPlay() {
		Application.LoadLevel("HowScene");
	}

	public void Back() {
		Application.LoadLevel("MenuScene");
	}
}
